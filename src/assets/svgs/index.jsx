export { default as Logo } from "./logo.svg";
export { default as Lightbible } from "./Light-bible.svg";
export { default as fbIcon } from "./facebook.svg";
export { default as igIcon } from "./instagram.svg";
export { default as xIcon } from "./twitter.svg";
export { default as users } from "./users.svg";
export { default as love } from "./love.svg";
export { default as ticket } from "./ticket.svg";
